package com.demo.springboot.rest;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.model.DataBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;


@RestController
@RequestMapping("/api")
public class MovieApiController {



    private final DataBase dataBase = new DataBase();

    @PostMapping("/users")
    public ResponseEntity<Void> createMovie(@RequestBody MovieDto movieDto) throws URISyntaxException {
        try {
            if (movieDto.getTitle() == null || movieDto.getYear() == null || movieDto.getImage() == null) {
                throw new Exception();
            }

            dataBase.addMovie(movieDto);
        } catch (Exception e) {

            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.created(new URI("/api/users/")).build();
    }

    @GetMapping("/users")
    public ResponseEntity<List<MovieDto>> getMovies() {

        return ResponseEntity.ok().body(dataBase.getMovieList());
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable Integer id, @RequestBody MovieDto movieDto) {

        try {
            dataBase.updateMovie(id, movieDto);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> getMovie(@PathVariable("id") Integer id) {
        try {

            dataBase.removeMovie(id);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }
}
