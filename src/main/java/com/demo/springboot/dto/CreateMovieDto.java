package com.demo.springboot.dto;

public class CreateMovieDto {
   // private Integer movieId;
    private String title;
    private Integer year;
    private String image;

    public CreateMovieDto() {
    }

    /*
    public Integer getMovieId() {
        return movieId;
    }
    */
    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public String getImage() {
        return image;
    }


}
